FROM golang:1.16.3-alpine

COPY ./ /go/src/evermos

WORKDIR /go/src/evermos

RUN go mod tidy
