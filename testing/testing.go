package testing

// import (
// 	"fmt"
// 	routes "evermos/app"
// 	ACCRepo ""
// 	ACCUsecase ""
// 	gorm ""
// 	"net/http"
// 	"net/http/httptest"
// 	"strings"
// 	"testing"

// 	"github.com/gin-gonic/gin"
// 	"github.com/stretchr/testify/assert"
// )

// // go test -v -run=TestByAccountNumber
// func TestByAccountNumber(t *testing.T) {
// 	r := gin.New()
// 	db := gorm.MysqlConn()
// 	art := ACCRepo.NewRepository(db)
// 	arti := ACCUsecase.NewUsecase(art)
// 	routes.AccountHttpHandler(r, arti)

// 	url := "localhost:10017/payment"

// 	expected := `{"id":1,"quantity":2,"status":"success"}`
// 	w := httptest.NewRecorder()
// 	req, err := http.NewRequest("POST", url, nil)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	r.ServeHTTP(w, req)
// 	fmt.Printf("Result %+v", w)

// 	assert.Equal(t, w.Code, http.StatusOK)
// 	assert.Equal(t, w.Body.String(), expected)
// }

// // go test -v -run=TestTransfer
// func TestTransfer(t *testing.T) {
// 	r := gin.New()
// 	db := gorm.MysqlConn()
// 	art := ACCRepo.NewRepository(db)

// 	arti := ACCUsecase.NewUsecase(art)
// 	routes.AccountHttpHandler(r, arti)

// 	payload := strings.NewReader(`{
// 		"transfer_to": "123451001",
// 		"transfer_amount": 1000000
// 	}`)

// 	url := "/api/v1/account/123451002/transfer"

// 	req, err := http.NewRequest("POST", url, payload)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	req.Header.Set("Content-Type", "application/json")
// 	rr := httptest.NewRecorder()

// 	r.ServeHTTP(rr, req)
// 	fmt.Printf("Result %+v", rr)

// 	if status := rr.Code; status != http.StatusCreated {
// 		t.Errorf("handler returned wrong status code: got %v want %v",
// 			status, http.StatusCreated)
// 	}
// 	expected := `{"code":201,"message":"Success","status":"Success"}`
// 	if rr.Body.String() != expected {
// 		t.Errorf("handler returned unexpected body: got %v want %v",
// 			rr.Body.String(), expected)
// 	}

// 	//assert.Equal(t, rr.Code, http.StatusOK)
// 	assert.Equal(t, rr.Body.String(), expected)
// }
