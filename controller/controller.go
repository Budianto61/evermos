package controller

import (
	"encoding/json"
	mdl "evermos/models"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type Controller struct {
	RDB *redis.Client
}

func (ctrl *Controller) Controller(c *gin.Context) {
	conn, ch, err := mdl.QueueConnection()
	defer conn.Close()
	defer ch.Close()
	if err != nil {
		panic(err)
	}

	// publish redis
	msgs, corrId, err := mdl.PublishRedis("check_stock", conn, ch, c)
	if err != nil {
		panic(err)
	}

	// get response from publish redis
	response := make(map[string]interface{})
	for d := range msgs {
		if corrId == d.CorrelationId {
			res := string(d.Body)
			err := json.Unmarshal([]byte(res), &response)
			if err != nil {
				panic(err)
			}
			break
		}
	}

	c.JSON(200, response)
}
