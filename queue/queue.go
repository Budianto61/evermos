package queue

import (
	"encoding/json"
	models "evermos/models"
	"fmt"
	"log"

	"github.com/go-redis/redis"
	"github.com/streadway/amqp"
)

func JobQueueRedis(rdb *redis.Client) {
	conn, ch, err := models.QueueConnection()
	defer conn.Close()
	defer ch.Close()
	if err != nil {
		fmt.Println(err)
	}

	// consume redis
	msgs, err := models.ConsumeRedis("check_stock", conn, ch)
	if err != nil {
		panic(err)
	}

	channel := make(chan bool)

	go func() {
		for d := range msgs {
			request := models.Request{}
			response := models.Response{}
			json.Unmarshal(d.Body, &request)

			available, err := models.RedisStockAvailable(rdb, 1, request)
			if available {
				models.ProduceUpdateDB("update_process", d.Body, conn, ch)
				response.Status = "success"
			} else {
				response.Status = "failed"
			}

			response.ID = request.ID
			response.Quantity = request.Quantity
			resBody, err := json.Marshal(response)
			models.HandlingError(err, "Failed marshal response")

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          resBody,
			})
			models.HandlingError(err, "Failed publish a message")

			d.Ack(false)
		}
	}()

	log.Printf(" [*] Waiting for job cek stock \n")
	<-channel
}

func JobUpdateDB(thread int) {
	conn, ch, err := models.QueueConnection()
	defer conn.Close()
	defer ch.Close()
	if err != nil {
		panic(err)
	}

	msgs, err := models.ConsumeUpdateDB("update_process", conn, ch)
	if err != nil {
		panic(err)
	}

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("Processing message update DB [thread %d]", thread)
			var request models.Request
			err := json.Unmarshal(d.Body, &request)
			if err != nil {
				panic(err)
			}
			models.ProcessingData(request)
		}
	}()

	log.Printf(" [*] Waiting for job update db [thread %d]", thread)
	<-forever
}
