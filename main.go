package main

import (
	ctrl "evermos/controller"
	mdl "evermos/models"
	que "evermos/queue"

	"github.com/gin-gonic/gin"
)

func main() {
	rdb := mdl.RedisConnection()
	// update data from db to redis
	mdl.UpdateStock(rdb)

	// consume data redis
	go que.JobQueueRedis(rdb)

	// update database
	for i := 1; i <= 10; i++ {
		go que.JobUpdateDB(i)
	}

	r := gin.Default()
	ctrl := ctrl.Controller{RDB: rdb}
	r.POST("/payment", ctrl.Controller)
	r.Run()
}
