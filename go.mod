module evermos

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.2.0
	github.com/onsi/gomega v1.12.0 // indirect
	github.com/streadway/amqp v1.0.0
	gorm.io/driver/mysql v1.0.6
	gorm.io/gorm v1.21.9
)
