CREATE TABLE `orders` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `order_quantity` int DEFAULT NULL,
  `order_status` int DEFAULT '0',
  `order_product_id` int DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_id_UNIQUE` (`order_id`)
);

CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(45) DEFAULT NULL,
  `product_stock` int DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_id_UNIQUE` (`product_id`)
);

INSERT INTO `product` (product_name, product_stock) 
VALUES ("Mouse", 100), ("Flashdisk", 200), ("Laptop", 20); 

INSERT INTO `orders` (order_quantity, order_status, order_product_id) 
VALUES (1, 0, 1), (2, 0, 3); 