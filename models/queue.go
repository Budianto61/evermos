package models

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/streadway/amqp"
)

// connection to rabbitmq
func QueueConnection() (
	conn *amqp.Connection,
	ch *amqp.Channel,
	err error,
) {
	conn, err = amqp.Dial(fmt.Sprintf(
		"amqp://%s:%s@%s:%s/",
		os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASS"),
		os.Getenv("RABBIT_HOST"), os.Getenv("RABBIT_PORT"),
	))
	HandlingError(err, "Failed connect to RabbitMQ")

	ch, err = conn.Channel()
	HandlingError(err, "Failed open a channel")
	return
}

// publish redis
func PublishRedis(routeKey string, conn *amqp.Connection, ch *amqp.Channel, c *gin.Context) (msgs <-chan amqp.Delivery, corrId string, err error) {
	que, err := ch.QueueDeclare("", false, false, true, false, nil)
	HandlingError(err, "Failed to declare a queue")

	msgs, err = ch.Consume(que.Name, "", true, false, false, false, nil)
	HandlingError(err, "Failed to register a consumer")

	corrId = uuid.NewString()
	body, err := c.GetRawData()

	if err != nil {
		fmt.Println(err)
	}

	err = ch.Publish("", routeKey, false, false, amqp.Publishing{
		ContentType:   "text/plain",
		CorrelationId: corrId,
		ReplyTo:       que.Name,
		Body:          body,
	})
	HandlingError(err, "Failed publish a message")
	return
}

//consume redis
func ConsumeRedis(routeKey string, conn *amqp.Connection, ch *amqp.Channel) (
	msgs <-chan amqp.Delivery,
	err error) {
	que, err := ch.QueueDeclare(routeKey, false, false, false, false, nil)
	HandlingError(err, "Failed declare a queue")

	err = ch.Qos(1, 0, false)
	HandlingError(err, "Failed set QoS")

	msgs, err = ch.Consume(que.Name, "", false, false, false, false, nil)
	HandlingError(err, "Failed register a consumer")
	return
}

// put to queue
func ProduceUpdateDB(
	qName string, body []byte, conn *amqp.Connection, ch *amqp.Channel,
) (err error) {
	q, err := ch.QueueDeclare(qName, false, false, false, false, nil)
	HandlingError(err, "Failed to declare a queue")

	err = ch.Publish("", q.Name, false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        body,
	})
	HandlingError(err, "Failed to publish a message")
	return
}

// get from queue and update to db
func ConsumeUpdateDB(
	qName string, conn *amqp.Connection, ch *amqp.Channel) (msgs <-chan amqp.Delivery, err error) {
	q, err := ch.QueueDeclare(qName, false, false, false, false, nil)
	HandlingError(err, "Failed declare a queue")
	msgs, err = ch.Consume(q.Name, "", true, false, false, false, nil)
	HandlingError(err, "Failed register a consumer")

	return
}

// error handling
func HandlingError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s\n", msg, err)
	}
}
