package models

import (
	"fmt"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func GetAllProduct() (Response []Product, err error) {
	root := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/evermos?charset=utf8mb4&parseTime=True&loc=Local",
		os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASS"),
		os.Getenv("MYSQL_HOST"), os.Getenv("MYSQL_PORT"),
	)

	db, err := gorm.Open(mysql.Open(root), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
	}

	sql, err := db.DB()
	if err != nil {
		return nil, err
	}
	defer sql.Close()

	res := db.Table("product").Find(&Response)
	if res.Error != nil {
		return nil, res.Error
	}
	return
}

// proces create data on checkout and update quantity on table product
func ProcessingData(data Request) (err error) {
	root := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/evermos?charset=utf8mb4&parseTime=True&loc=Local",
		os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASS"),
		os.Getenv("MYSQL_HOST"), os.Getenv("MYSQL_PORT"),
	)
	db, err := gorm.Open(mysql.
		Open(root), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
	}
	sql, err := db.DB()
	if err != nil {
		return err
	}
	defer sql.Close()

	// create data order
	trx := Order{
		OrderID:       data.ID,
		OrderQuantity: data.Quantity,
		OrderStatus:   0,
		CreatedDate:   time.Now(),
	}
	res := db.Table("orders").Create(&trx)
	if res.Error != nil {
		return res.Error
	}

	// process choice
	product := Product{ProductID: int(data.ID)}
	res = db.Table("product").
		Model(&product).
		Update("product_stock", gorm.Expr("product_stock - ?", data.Quantity))
	if res.Error != nil {
		return res.Error
	}

	return
}
