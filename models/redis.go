package models

import (
	"fmt"
	"os"
	"strconv"

	"github.com/go-redis/redis"
)

// create redis connection
func RedisConnection() (rdb *redis.Client) {
	rdb = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	return
}

// update stock data from db to redis
func UpdateStock(rdb *redis.Client) {
	product, err := GetAllProduct()
	if err != nil {
		fmt.Println(err)
	}
	for _, oh := range product {
		err := rdb.Set(strconv.FormatInt(int64(oh.ProductID), 10), oh.ProductStock, 0).Err()
		if err != nil {
			fmt.Println(err)
		}
	}
}

// check stock available quantity from redis
func RedisStockAvailable(rdb *redis.Client, key int, data Request) (available bool, err error) {
	rKey := strconv.FormatInt(int64(key), 10)
	stock, err := rdb.Get(rKey).Int()
	if err != nil {
		return
	}
	if (stock - data.Quantity) >= 0 {
		err = rdb.Set(rKey, stock-data.Quantity, 0).Err()
		if err != nil {
			return false, err
		}
		return true, nil
	}
	return false, nil
}
