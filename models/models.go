package models

import "time"

type (
	Product struct {
		ProductID    int `gorm:"primaryKey"`
		ProductName  string
		ProductStock int
		CreatedDate  time.Time
	}

	Order struct {
		OrderID        int `gorm:"primaryKey"`
		OrderQuantity  int
		OrderStatus    int
		OrderProductID int
		CreatedDate    time.Time
	}

	Request struct {
		ID       int `json:"id"`
		Quantity int `json:"quantity"`
	}

	Response struct {
		ID       int    `json:"id"`
		Quantity int    `json:"quantity"`
		Status   string `json:"status"`
	}
)
